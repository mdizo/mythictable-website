const config = {
	plugins: [require('postcss-custom-media'), require('autoprefixer')]
};

module.exports = config;
